
#include <stdio.h>
#include <string.h>
#include  <signal.h>
#include <stdlib.h>
#include <math.h>

#include <time.h>

#include <udpcast.h>

#include <wiringPi.h>
#include <mcp23017.h>

#include <pthread.h>

#define ALARM_DELAI 2

char *cammacs[7]={
"d4:d9:19:49:28:8a", // OK 1   ->  1
"d8:96:85:56:72:7f", // OK -> 2
"d4:d9:19:49:19:fb", // OK 6   ->  3
"d4:d9:19:49:40:54", // OK 2   ->  4
"d4:d9:19:47:1a:21", // OK 4   ->  5
"d4:d9:19:47:d7:d1", // OK 3   ->  6
NULL};


//"d4:d9:19:49:32:08", // plus active



udpcast udpin;
udpcast udpout;

void dump(unsigned char *b,int len) {
    int i;
    for(i=0;i<len;i++) printf(" %02x",b[i]);
    printf("\n");
}



volatile int compte=0;
volatile int call=0;

volatile int nbcam=0; // total nb cam connected

volatile int recording;
volatile double recordStartTime;

// les connections
#define MAX_C   20
char *connectMac[MAX_C]; // mac address de la station
char *connectIP[MAX_C]; // ip de la station
udpcast udpoutC[MAX_C]; // tous les udp out

FILE *lcd;

void init() {
    int i;
    for(i=0;i<MAX_C;i++) connectMac[i]=NULL;
    for(i=0;i<MAX_C;i++) connectIP[i]=NULL;
    recording=0;
    nbcam=0;
    lcd=popen("/usr/bin/python /home/pi/bin/info.py >/dev/null","w");
    if( lcd==NULL ) { printf("no lcd!\n");exit(0); }
    fprintf(lcd,"                                ");
    fflush(lcd);
}

void afficheCam() {
int j,nn;
    nn=0;
    for(j=0;j<MAX_C;j++) {
        if( connectMac[j]==NULL ) continue;
	nn++;
        printf("%d : mac=%s ip=%s\n",j,connectMac[j],connectIP[j]);
    }
    nbcam=nn;
    /*
    char cmd[100];
    sprintf(cmd,"message.py \"%d camera%c\"",nb,(nb>1)?'s':' ');
    system(cmd);
    */
}

void verifyNewConnections()
{
    int k;
    char buf[200];
    char a[100],mac[100],ip[100],nom[100];
    char *newMac[MAX_C];

    int i;
    for(i=0;i<MAX_C;i++) newMac[i]=NULL;

    // lire les nouvelles mac

    FILE *P=popen("iw wlan0 station dump","r");
    if( P==NULL ) { printf("!!!!!!\n");exit(0); }
    int j=0;
    for(;;) {
        k=fscanf(P," %[^\n]",buf);
        if( k!=1 ) break;
        if( strncmp("Station",buf,6)!=0 ) continue;
        sscanf(buf,"Station %s ",mac);
        newMac[j]=strdup(mac);
        j++;
    }
    fclose(P);

    //for(i=0;newMac[i];i++) printf("MAC ACTIVE = %s\n",newMac[i]);

    // ferme tout ce qui n'est pas dans newMac
    for(i=0;i<MAX_C;i++) {
        if( connectMac[i]==NULL ) continue;
        for(j=0;newMac[j];j++) if( strcmp(connectMac[i],newMac[j])==0 ) break;
        if( newMac[j]==0 ) {
            // ferme [i]
            printf("Enleve MAC %s\n",connectMac[i]);
            if( connectMac[i] ) free(connectMac[i]);
            connectMac[i]=NULL;
            if( connectIP[i] ) free(connectIP[i]);
            connectIP[i]=NULL;
            udp_uninit_sender(udpoutC+i);
            afficheCam();
        }else{
            //printf("MAC %s DEJA OUVERT\n",connectMac[i]);
        }
    }

    // ajoute si necessaire les nouvelles MAC
    for(i=0;newMac[i];i++) {
        for(j=0;j<MAX_C;j++) {
            if( connectMac[j]!=NULL && strcmp(connectMac[j],newMac[i])==0 ) break;
        }
        if( j<MAX_C ) continue; // on a trouve la mac
        // ajoute la mac!
        // trouve un espace vide
        for(j=0;j<MAX_C;j++) if( connectMac[j]==NULL ) break;
        //printf("Ajoute mac %s pos %d\n",newMac[i],j);
        connectMac[j]=strdup(newMac[i]); // connectIP sera NULL
    }

    for(i=0;newMac[i];i++) { free(newMac[i]);newMac[i]=NULL; }


    FILE *F=fopen("/var/lib/misc/dnsmasq.leases","r");
    if( F==NULL ) { printf("ERROR!\n"); exit(0); }
    for(;;) {
        k=fscanf(F,"%s %s %s %s %s",a,mac,ip,nom,a);
        if( k!=5 ) break;
        //printf("CONNECTION %s  %s\n",mac,ip);

        // trouve la connection, ajoute si necessaire
        int i;
        for(i=0;i<MAX_C;i++) {
            if( connectMac[i]==NULL ) continue;
            if( strcmp(connectMac[i],mac)==0 ) break;
        }
        if( i==MAX_C || connectIP[i]!=NULL ) continue;

        connectIP[i]=strdup(ip);
        udp_init_sender(udpoutC+i,ip,8484,UDP_TYPE_NORMAL);
        printf("NOUVELLE IP %s %s\n",connectMac[i],connectIP[i]);
        afficheCam();
    }
    fclose(F);

    /*
    for(j=0;j<MAX_C;j++) {
        if( connectMac[j]==NULL ) continue;
        printf("%d : mac=%s ip=%s\n",j,connectMac[j],connectIP[j]);
    }
    */
}





void send_wt(char *cmd) {
    unsigned char buf[13];
    int i;
    for(i=0;i<13;i++) buf[i]=0;
    buf[8]=1;
    buf[9]=(compte/256)&0xff;
    buf[10]=compte&0xff;
    buf[11]=cmd[0];
    buf[12]=cmd[1];
    printf("%s --> --> --> ",cmd);dump(buf,13);
    udp_send_data(&udpout,buf,13);
    compte++;
}

int sendNum=0;

void send_camera_info() {
    unsigned char buf[40];
    int i=sendNum;
    do {
        sendNum=(sendNum+1)%MAX_C;
    } while( sendNum!=i && connectMac[sendNum]==NULL );
    if( connectMac[sendNum]==NULL ) return;
    
    // ne pas envoyer... ca reset les cameras...
    //printf("send cam %d\n",sendNum);
    //sprintf(buf,"%s\n",connectIP[sendNum]);
    //udp_send_data(&udpout,buf,strlen(buf));
}


// send a tout le monde
// cmd: 'st' ou 'pw'
void send_pw_st(char *cmd,int param) {
    unsigned char buf[13];
    int i,j;
    for(i=0;i<MAX_C;i++) {
        for(j=0;j<13;j++) buf[j]=0;
        buf[8]=param;
        buf[9]=(compte/256)&0xff;
        buf[10]=compte&0xff;
        buf[11]=cmd[0];
        buf[12]=cmd[1];
        if( connectMac[i]==NULL ) continue;
        printf("%s --> %s -> ",cmd,connectIP[i]);dump(buf,13);
        udp_send_data(udpoutC+i,buf,13);
        compte++;
    }
}

// send a tout le monde
void send_CM(int mode) {
    unsigned char buf[14];
    int i,j;
    for(i=0;i<MAX_C;i++) {
        for(j=0;j<14;j++) buf[j]=0;
        buf[8]=0;
        buf[9]=(compte/256)&0xff;
        buf[10]=compte&0xff;
        buf[11]='C';
        buf[12]='M';
        buf[13]=mode;
        if( connectMac[i]==NULL ) continue;
        printf("CM --> %s -> ",connectIP[i]);dump(buf,14);
        udp_send_data(udpoutC+i,buf,14);
        compte++;
    }
}


// send a tout le monde
// state: 1=start, 0=stop
void send_SH(int state) {
    unsigned char buf[14];
    int i,j;
    for(i=0;i<MAX_C;i++) {
        for(j=0;j<14;j++) buf[j]=0;
        buf[8]=0;
        buf[9]=(compte/256)&0xff;
        buf[10]=compte&0xff;
        buf[11]='S';
        buf[12]='H';
        buf[13]=state;
        if( connectMac[i]==NULL ) continue;
        printf("SH --> %s -> ",connectIP[i]);dump(buf,14);
        int k=udp_send_data(udpoutC+i,buf,14);
        printf("status %d\n",k);
        // TEST!
        //udp_send_data(&udpout,buf,14);
        compte++;
    }
}

// send a tout le monde
// state: 1=on, 0=off
void send_PW(int state) {
    unsigned char buf[14];
    int i,j;
    for(i=0;i<MAX_C;i++) {
        for(j=0;j<14;j++) buf[j]=0;
        buf[8]=0;
        buf[9]=(compte/256)&0xff;
        buf[10]=compte&0xff;
        buf[11]='P';
        buf[12]='W';
        buf[13]=state;
        if( connectMac[i]==NULL ) continue;
        printf("PW --> %s -> ",connectIP[i]);dump(buf,14);
        udp_send_data(udpoutC+i,buf,14);
        compte++;
    }
}


int mode=0;

void handle(int sig)
{
  signal(SIGALRM, SIG_IGN);          /* ignore this signal       */
  //printf("---- %d ----\n",call);

  verifyNewConnections();
  send_camera_info();

  send_wt("wt");
  //send_pw_st("pw",1);
  //send_pw_st("st",0);
  /*
  if( call==1 ) {
      send_CM(0);
  }
  if( call==4 ) {
      send_SH(1);
  }
  if( call==8 ) {
      send_SH(0);
  }
  */

  signal(SIGALRM, handle);     /* reinstall the handler    */
  alarm(ALARM_DELAI);
  call++;
}

double now() {
    struct timeval tv;
    gettimeofday(&tv, 0);
    return tv.tv_sec+tv.tv_usec/1000000.0;
}


void message() {
    char cmd[100];
    char extra[100];
    int yo=(int)floor(now()-recordStartTime);
    if( recording ) sprintf(extra,"%02d:%02d REC",yo/60,yo%60);
    else strcpy(extra,"");
    //printf("--> %s %d %f %f %d\n",extra,recording,recordStartTime,now(),yo);
    
    char camOnOff[20];
    int i,j;
    for(i=0;cammacs[i];i++) {
        camOnOff[i]='-';
        for(j=0;j<MAX_C;j++) if( connectMac[j]!=NULL && strcmp(connectMac[j],cammacs[i])==0 ) camOnOff[i]='1'+i;

    }
    camOnOff[i]=0;
   
    sprintf(cmd,"%1d camera%c %s",nbcam,(nbcam>1)?'s':' ',camOnOff);
    //system(cmd);
    printf("cmd -> %s : %s\n",cmd,extra);
    fprintf(lcd,"%-16s%-16s\n",cmd,extra);
    fflush(lcd);
}


void startRecording() {
                printf("Start recording\n");
                send_SH(1);
                recording=1;
                recordStartTime=now();
                message();
}

void stopRecording() {
                printf("Stop  recording\n");
                send_SH(0);
                recording=0;
                message();
}


void *button_thread(void *x)
{
	mcp23017Setup (100, 0x20) ;
	int i;
	for(i=0;i<5;i++) pinMode(100+i,INPUT);

	for(i=0;i<5;i++) pullUpDnControl(100+i,PUD_UP);

    int lastb=0;
    double lastRefresh=0;
	
    int cc=0;

    message();
    lastRefresh=now();

	for(;;cc++) {
        int b,mask,sel;
		for(i=0,b=0,mask=1;i<5;i++,mask<<=1) if( digitalRead(100+i)==0 ) b|=mask;
        sel=b^lastb;
        if( sel ) {
            mask=b&sel; // mask: 1=pressed, 0=release,   sel: 1=changed, 0 no change
            printf("%2d %3d %12.6f\n",sel,mask,now());

            if( (sel&1) && (mask&1)==0 ) {
                startRecording();
            }
            if( (sel&2) && (mask&2)==0 ) {
                stopRecording();
            }
        }
        lastb=b;
		usleep(50000); // 0.05s
        double delta=now()-lastRefresh;
        if( cc==0 || (recording && delta>=1.0) || delta>=5.0 ) {
            lastRefresh=now();
            message();
        }
	}
}



int main(int argc,char *argv[])
{


	unsigned char cmd[2000];

    init();


    pthread_t buttons;
    if(pthread_create(&buttons, NULL, button_thread, NULL)) {
        fprintf(stderr, "Error creating thread\n");
        return 1;
    }



    udp_init_sender(&udpout,"10.71.79.255",8484,UDP_TYPE_BROADCAST);

    udp_init_receiver(&udpin,8484,NULL); 

    int k;

    printf("go\n");
    signal(SIGALRM, handle);     /* reinstall the handler    */
    alarm(ALARM_DELAI);

    for(;;) {
        if( (k=udp_receive_data(&udpin,cmd,500))>0 ) {
            if( k<=0 ) continue;
            unsigned char *nip=(unsigned char *)&udpin.saddr.sin_addr.s_addr;
            printf("recu de %d.%d.%d.%d ",nip[0],nip[1],nip[2],nip[3]); dump(cmd,k);
            //process(b,k);
            if( cmd[0]=='m' ) send_CM(cmd[1]-'0'); // mode
            if( cmd[0]=='s' && cmd[1]=='1' ) startRecording();
            if( cmd[0]=='s' && cmd[1]=='0' ) stopRecording();
            if( cmd[0]=='p' && cmd[1]=='0' ) {
                send_PW(cmd[1]-'0');
                send_PW(cmd[1]-'0');
                send_PW(cmd[1]-'0');
            }
            if( cmd[0]=='s' && cmd[1]=='d' ) {
                fprintf(lcd,"@@@ SHUTDOWN @@@                ");
                fflush(lcd);
                send_PW(0);
                send_PW(0);
                send_PW(0);
                system("/sbin/shutdown now"); // "myshutdown"
            }
        }
    }

    /**
    for(;;) {
        printf("m0  : set mode 0 (0,1,2,3,6)\n");
        printf("s1 s0  : record start/stop\n");
        printf("p0  : power off\n");
        printf("p1  : power on\n");
        printf("q    : quit\n");
        printf("cmd:");
        char cmd[100];
        scanf(" %s",cmd);
        printf("cmd=%s\n",cmd);
        if( cmd[0]=='q' ) break;
        if( cmd[0]=='m' ) send_CM(cmd[1]-'0');
        if( cmd[0]=='s' ) send_SH(cmd[1]-'0');
        if( cmd[0]=='p' && cmd[1]=='0' ) { send_PW(cmd[1]-'0'); }
        if( cmd[0]=='p' && cmd[1]=='1' ) {
            send_pw_st("cv",0);
            //send_wt("cv");
            //send_wt("wt");
            //send_wt("wt");
            //send_pw_st("pw",1);
            //send_pw_st("st",0);
        }
    }
    **/



}



