


#include <stdio.h>
#include <string.h>

#include <udpcast.h>


typedef struct {
     unsigned char cm[2]; // CM=[0,1],
     unsigned char oo[1]; // OO=[0],
     unsigned char wt[1]; // wt=[0],
     unsigned char se[31]; //se=[0 for i in range(31)],
     unsigned char cv[1]; // cv==[1]
     unsigned char PW[1]; // PW=[0] ?????? on recoit [13]=0 quand on demande power off
     unsigned char pw[2]; // pw=[0,1] 
     unsigned char st[5]; // st=[0,0,0,0,0]
     unsigned char sh[2]; // SH=[0,0]):
} status;


status s;

udpcast udpin;
udpcast udpout;

void initStatus() {
    int i;
    s.cm[0]=0; s.cm[1]=1;
    s.oo[0]=0;
    s.wt[0]=0;
    for(i=0;i<31;i++) s.se[i]=0;
    s.cv[0]=2;
    s.pw[0]=0; s.pw[1]=1; // [0]0=OK,1=standby/busy [1]:1=ON, 0=i am off
    for(i=0;i<5;i++) s.st[i]=0;   //-> [0]? [1]mode [2]recording [3]? [4]recording?
    s.sh[0]=0;s.sh[1]=0;
}


int process(unsigned char *b,int len) {
    int i;
    printf("got: ");
    for(i=0;i<len;i++) printf(" %02x",b[i]);
    printf(" %c%c",b[11],b[12]);
    printf("\n");

    char cmd[2];
    cmd[0]=b[11];
    cmd[1]=b[12];

    unsigned char reponse[1000];
    int rlen;

    memcpy(reponse,b,13);
    rlen=13;

    if( cmd[0]=='C' && cmd[1]=='M' ) {
        int mode=b[13];
        printf("nouveau mode : %d\n",mode); // mode 0 = video
        s.st[1]=mode;
        memcpy(reponse+rlen,s.cm,2);rlen+=2;

    }else if( cmd[0]=='O' && cmd[1]=='O' ) {
        memcpy(reponse+rlen,s.oo,1);rlen+=1;

    }else if( cmd[0]=='w' && cmd[1]=='t' ) {
        memcpy(reponse+rlen,s.wt,1);rlen+=1;

    }else if( cmd[0]=='s' && cmd[1]=='e' ) {
        memcpy(reponse+rlen,s.se,31);rlen+=31;

    }else if( cmd[0]=='c' && cmd[1]=='v' ) {
        memcpy(reponse+rlen,s.cv,1);rlen+=1;

    }else if( cmd[0]=='P' && cmd[1]=='W' ) {
        int poweron=b[13]; // 0=poweroff!
        printf("power on = %d\n",poweron);
        memcpy(reponse+rlen,s.PW,1);rlen+=1;

    }else if( cmd[0]=='p' && cmd[1]=='w' ) {
        memcpy(reponse+rlen,s.pw,2);rlen+=2;

    }else if( cmd[0]=='s' && cmd[1]=='t' ) {
        memcpy(reponse+rlen,s.st,5);rlen+=5;

    }else if( cmd[0]=='S' && cmd[1]=='H' ) {
        int start=b[13]; // 1=start, 0=stop
        s.st[2]=start;
        s.st[4]=start;
        memcpy(reponse+rlen,s.sh,2);rlen+=2;
    }

    udp_send_data(&udpout,reponse,rlen);

}


int main(int argc,char *argv[])
{

    unsigned char b[2000];

    udp_init_sender(&udpout,"10.71.79.1",8484,UDP_TYPE_NORMAL);

    udp_init_receiver(&udpin,8484,NULL); 

    int k;
    initStatus();

    for(;;) {
        if( (k=udp_receive_data(&udpin,b,500))>0 ) {
            process(b,k);
        }
    }



}



