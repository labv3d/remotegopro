#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <termios.h>


#define RES 46.2857 // en secondes d'arc
#define STEP_PAR_DEG 3600/46.2857

#define MINPAN -12359
#define MAXPAN 12359
#define MINTILT -3644
#define MAXTILT 2429

#define A2P(a) ((a)*(STEP_PAR_DEG))
#define P2A(p) ((p)/(STEP_PAR_DEG))

int pt_init( char *device );

int pt_sendcmd( int file, char* buf, char* answer, int answer_size );
int pt_sendcmd_int( int file, char* cmd, int value );

int pt_getstatus( int file, char* cmd );

int pt_waitloop( int file, char* cmd, int value );
