#include "pt.h"

//#define RESET


#define SZ  500
int pt;
char answer[SZ];

int pan(int pos) {
	pt_sendcmd_int(pt, "PP", pos);
	pt_waitloop(pt, "PP", pos);
}

int tilt(int pos) {
	pt_sendcmd_int(pt, "TP", pos);
	pt_waitloop(pt, "TP", pos);
}

int where() {
    pt_sendcmd(pt, "PP", answer, SZ);
    printf("pan at %s\n", answer);
    pt_sendcmd(pt, "TP", answer, SZ);
    printf("tilt at %s\n", answer);
}

int main( int argc, char **argv ) {


	if (argc>1) {
		printf("Initializing %s...\n", argv[1]);
		pt = pt_init(argv[1]);
	} else {
		printf("Initializing %s...\n", "/dev/tty.KeySerial1");
		//pt = pt_init("/dev/tty.KeySerial1");
		pt = pt_init("/dev/ttyUSB0");
	}
	printf("Initialization done\n");

#ifdef RESET
    // reset tilt
    printf("reset tilt\n");
	pt_sendcmd(pt, "RT",NULL,0);

    // reset pan
    printf("reset pan\n");
	pt_sendcmd(pt, "RP",NULL,0);

    // disable/enable reset on power up (RD=non, RT=tilt only,RP =pan only, RE=all)
    printf("disable reset on power on\n");
	pt_sendcmd(pt, "RD",NULL,0);
	pt_sendcmd(pt, "RE",NULL,0);
    //exit(0);
    
    // Half step for tilt and pan
	//pt_sendcmd(pt, "WTH", answer, SZ);
	//pt_sendcmd(pt, "WPH", answer, SZ);
    
    // ceci est le choix actuel
    // quarter step for tilt and pan
    printf("quarter step\n");
	pt_sendcmd(pt, "WTQ", answer, SZ);
	pt_sendcmd(pt, "WPQ", answer, SZ);
    exit(0);
    
#endif
	pt_sendcmd(pt, "PN", answer, SZ);
    printf("pan min = %s\n",answer);
    exit(0);

    // disable pan position limit
	pt_sendcmd(pt, "LD", answer, SZ);
	pt_sendcmd(pt, "TD", answer, SZ);

	pt_sendcmd(pt, "L", answer, SZ);
    printf("pan limit mode = %s\n",answer);
    
    /// ATTENTION, ne ffonctionne pas si limite mode disabled...
    ///// recoit deux lignes: un avertissement et la valeur
	pt_sendcmd(pt, "PN", answer, SZ);
    printf("pan min = %s\n",answer);
	pt_sendcmd(pt, "PX", answer, SZ);
    printf("pan max = %s\n",answer);

	pt_sendcmd(pt, "PR", answer, SZ);
    printf("pan resolution = %s\n",answer);


    int stepsPerTurn=(int)(360.0*3600.0/atof(answer)+0.5);
    printf("pan steps per turn = %d\n",stepsPerTurn);

    // tilt
	pt_sendcmd(pt, "TN", answer, 90);
    printf("tilt min = %s\n",answer);
	pt_sendcmd(pt, "TX", answer, 90);
    printf("tilt max = %s\n",answer);
	pt_sendcmd(pt, "TR", answer, 90);
    printf("tilt reoslution = %s\n",answer);


    // speed (steps/sec)
    printf("setting speed to %d\n",stepsPerTurn*10/360);
	pt_sendcmd_int(pt, "PS", stepsPerTurn*10/360); // 10 deg per sec

    // acceleration (steps/sec^2)
    printf("setting acceleration to %d\n",stepsPerTurn*2/360);
	pt_sendcmd_int(pt, "PA", stepsPerTurn*2/360); // 1 deg per sec / sec


    exit(0);

    //tilt(stepsPerTurn*(-7235)/360);
    tilt(0);//-7000);
    //tilt(4822);
    printf("%d\n",stepsPerTurn*45/360);

    /*
    tilt(0);
    pan(stepsPerTurn*(-180)/360);
    system("/home/pi/bin/start-record");
    sleep(2);
    pan(stepsPerTurn*(180)/360);
    tilt(-7000);
    pan(stepsPerTurn*(-180)/360);
    system("/home/pi/bin/stop-record");
    tilt(0);
    sleep(5);
    */

    tilt(0);
    //pan(stepsPerTurn*(-180)/360);
    pan(-10000);
    //system("/home/pi/bin/start-record");
    int pos;
    for(pos=-180;pos<180;pos+=30) {
        printf("Trying %d\n",pos);
        where();
        pan(stepsPerTurn*pos/360);
        //system("/home/pi/bin/start-record");
        sleep(5);
        //system("/home/pi/bin/stop-record");
        where();
        sleep(1);
    }
    //system("/home/pi/bin/stop-record");

    where();
    pan(0);
    tilt(0);
    where();




	return 1;
}
