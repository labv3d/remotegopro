#!/usr/bin/tclsh8.6


exec stty -F /dev/ttyUSB0 raw -echo 9600
#exec stty -F /dev/ttyACM0 raw -echo 9600



# en secondes d'arc
set res 46.2857
set stepParDeg [expr 3600/$res]

puts "step par degre = $stepParDeg"


proc a2p {a} {
	global stepParDeg
	return [expr int($a*$stepParDeg)]
}

proc p2a {p} {
	global stepParDeg
	return [expr $p/$stepParDeg]
}


set pmin  -12367
set pmax  12367
set amin [p2a $pmin]
set amax [p2a $pmax]

puts "angle 100 = [a2p 100] pos = [p2a [a2p 100]] deg"
puts "angles min $amin max $amax"


proc sendcmd {f cmd} {
	puts $f $cmd
	flush $f
	for {} {1} {} {
		after 250
		set k [gets $f line]
		#puts "cmd $cmd : got k=$k line=>>>$line<<<"
		if { $k>0 } {
			return $line
		}
	}
}


proc sendcmdint {f cmd} {
	puts $f $cmd
	flush $f
	after 250
	set k [gets $f line]
	#puts "cmd $cmd : got k=$k line=>>>$line<<<"
	scan $line " * %d" pos
	return $pos
}



proc attendre {f cmd res} {
	for {} {1} {} {
		puts "attendre $res... commande $cmd pos $res"
		for {set k 1} {$k>0} {} {
		    set line [sendcmd $f $cmd]
            set k [string length $line]
			#puts "got k=$k line=$line"
			if { $k>0 } {
				set pos "99999999"
				scan $line "* %d" pos
				if { $pos == $res } { return }
			}
		}
		after 500
	}
}

set f [open "/dev/ttyUSB0" RDWR]

fconfigure $f -blocking 0 -buffering line


# terse output mode
sendcmd $f "ft"
# disable echo of command
sendcmd $f "ed"

#puts [a2p -155]
#puts [a2p 155]

### query resolution
set panmin [sendcmdint $f "pn"]
set panmax [sendcmdint $f "px"]
set tiltmin [sendcmdint $f "tn"]
set tiltmax [sendcmdint $f "tx"]

puts "pan  : $panmin ... $panmax"
puts "tilt : $tiltmin ... $tiltmax"

#
# speed
#
set panspeed [sendcmdint $f "ps"]
set tiltspeed [sendcmdint $f "ts"]
puts "speed: pan=$panspeed, tilt=$tiltspeed (pos/sec)"
set panbasespeed [sendcmdint $f "pb"]
set tiltbasespeed [sendcmdint $f "tb"]
puts "speed (base): pan=$panbasespeed, tilt=$tiltbasespeed (pos/sec)"
set panmaxspeed [sendcmdint $f "pu"]
set tiltmaxspeed [sendcmdint $f "tu"]
puts "speed (max): pan=$panmaxspeed, tilt=$tiltmaxspeed (pos/sec)"

#
# acceleration
#
set panaccel [sendcmdint $f "pa"]
set tiltacel [sendcmdint $f "ta"]
puts "acceleration: pan=$panspeed, tilt=$tiltspeed (pos/sec^2)"

#
# resolution
#
# wpf : -6181 .. 6181
# wph : -12364 ... 12364 : 46.287  arcsec/step
#      tour complet: -14000...14000
# wpq : -24734 .. 24735  : 23.1428 arcsec/step
# wpe :
# wpa :
#
#sendcmd $f "wph"

#sendcmd $f "H"

set panres [sendcmd $f "pr"]
set tiltres [sendcmd $f "tr"]
puts "resolution: pan =$panres"
puts "resolution: tilt=$tiltres"

if { ![string match "*46.28*" $panres] } {
    puts "Need reset!!!"
    sendcmd $f "wph"
    sendcmd $f "H"
    exit
}

# disable position limit
sendcmd $f "ld"

#sendcmd $f "rp"
#sendcmd $f "ps6000"


#sendcmd $f "pp-14400"
#sendcmd $f "a"


#sendcmd $f "pp14400"
#sendcmd $f "a"

#sendcmd $f "pp$panmax"
#sendcmd $f "a"


#sendcmd $f "pp0"
#sendcmd $f "a"

#set pos [sendcmdint $f "pp"]
#puts "position is $pos"

set speed [a2p 20]
sendcmd $f "ps$speed"

puts "vers 0"
sendcmd $f "pp0"

#set speed [a2p 10]
#sendcmd $f "ps$speed"
#
#sendcmd $f "pp[a2p -40]"
#sendcmd $f "a"
#
#
#set speed [a2p 5]
#sendcmd $f "ps$speed"
#
#puts "vers 0..."
#sendcmd $f "pp0"
#sendcmd $f "a"


#puts "vers -180...[a2p -180]"
#sendcmd $f "pp[a2p -180]"
#sendcmd $f "a"

#exit

#foreach k { -60 60 } {
#	set p [a2p $k]
#	puts "going to $k deg = $p pos"
#	sendcmd $f "pp$p"
#	sendcmd $f "a"
#}


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#set freq { 440 466 494 523 554 587 622 660 698 740 784 831 880 }

#foreach k $freq {
#	sendcmd $f "ps$k"
#	sendcmd $f "pp$k"
#	sendcmd $f "a"
#	sendcmd $f "pp-$k"
#	sendcmd $f "a"
#}

#exit 
#sendcmd $f "ps660"
#
#sendcmd $f "pp660"
#sendcmd $f "a"
#
#sendcmd $f "ps440"
#
#sendcmd $f "pp0"
#sendcmd $f "a"



set pos [sendcmdint $f "pp"]
puts "position is $pos"

set depart -180
set arrivee 180
set step 90

set speed [a2p 10]
sendcmd $f "ps$speed"

for {set angle $depart} {$angle <= $arrivee} {set angle [expr $angle+$step]} {
	puts "angle = $angle"
	set p [a2p $angle]
	puts "pp$p"
	sendcmd $f "pp$p"
	attendre $f pp $p

	puts "nous sommes en $angle (pos=$p)"
	#set name [format "out%04d.ppm" [expr $angle-$depart]]
	#catch {exec imguCapture -o $name}
	after 1500
}




