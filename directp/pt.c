#include "pt.h"
#include <termios.h>

//#define IOCTL_SETUP "stty -F /dev/ttyACM0 raw -echo"
#define IOCTL_DEV_1 "/dev/ttyACM0"
#define IOCTL_DEV_2 "/dev/ttyUSB0"

// S'assure de recevoir l'echo
int pt_setup(char *device) {

	char setup[50];
#ifdef __APPLE__
	sprintf(setup, "stty -f %s raw -echo speed 9600", device);
	system(setup);
#else
	sprintf(setup, "stty -F %s raw -echo 9600", device);
	system(setup);
#endif
	
	return 1;
}

// Initialise la connection avec le device pantilt
int pt_init(char *device) {
	int pt_file;

#ifdef __APPLE__
	if (device==NULL) {
		printf("There are no default serial port, please specify a device\n");
		return -1;
	}
#endif

	char setup[50];
	if (device!=NULL) {
#ifndef __APPLE__
		pt_setup(device);
#endif

#ifdef __APPLE__
		pt_file = open(device, O_RDWR | O_NOCTTY | O_NDELAY);
#else
		pt_file = open(device, O_RDWR);
#endif
		
		if (pt_file < 0) {
			printf("Could not open device: %s \n", device);
			return -1;
		}
		printf("Successfully opened device: %s\n", device);
	
#ifdef __APPLE__
		// reset serial port parameters
		
		fcntl(pt_file, F_SETFL,0);
		tcflush(pt_file,TCIFLUSH); 
		/*
		  speed 9600 baud; 0 rows; 0 columns;
		  lflags: -icanon -isig -iexten -echo -echoe -echok -echoke -echonl
		  -echoctl -echoprt -altwerase -noflsh -tostop -flusho -pendin
		  -nokerninfo -extproc
		  iflags: -istrip -icrnl -inlcr -igncr -ixon -ixoff -ixany -imaxbel -iutf8 -ignbrk -brkint -inpck -ignpar -parmrk
		  oflags: -opost -onlcr -oxtabs -onocr -onlret
		  cflags: cread cs8 -parenb -parodd hupcl -clocal -cstopb -crtscts -dsrflow -dtrflow -mdmbuf
		  cchars: discard = ^O; dsusp = ^Y; eof = ^D; eol = <undef>;
		  eol2 = <undef>; erase = ^?; intr = ^C; kill = ^U; lnext = ^V;
		  min = 1; quit = ^\; reprint = ^R; start = ^Q; status = ^T;
		  stop = ^S; susp = ^Z; time = 0; werase = ^W;
		 */		


		struct termios options;
		printf("Setting Parameters...\n");
		tcgetattr(pt_file, &options);
		cfsetispeed(&options, B9600); //Set the baud rates to 9600
		cfsetospeed(&options, B9600);

		options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | IGNCR | IXOFF | IXANY | IMAXBEL |
							 IUTF8 | IGNPAR | INLCR | PARMRK | INPCK | ISTRIP | IXON);

		// Enable the receiver and set local mode
		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag &= ~PARENB; // Mask the character size to 8 bits, no parity
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8; // Select 8 data bits
		options.c_cflag &= ~CRTSCTS; // Disable hardware flow control
		options.c_cflag &= ~(PARODD | MDMBUF);
		options.c_cflag |= (HUPCL);
		// Enable data to be processed as raw input 
		options.c_lflag &= ~(ICANON | ECHO | ISIG);
		options.c_lflag &= ~(IEXTEN | ECHOK | ECHOKE | ECHONL | ECHOCTL | ECHOPRT);
		options.c_lflag &= ~(ALTWERASE | NOFLSH | TOSTOP | FLUSHO | PENDIN | NOKERNINFO | EXTPROC);

		options.c_oflag &= ~(OPOST | ONLCR | OXTABS | ONOCR | ONLRET);

		options.c_cc[VMIN]=0;//it will wait for one byte at a time.
		options.c_cc[VTIME]=10;// it will wait for 0.1s at a time.
		// Set the new options for the port 
		tcsetattr(pt_file, TCSANOW, &options);
		printf("Parameters SET!!!...\n");  
#endif
	}
	
	if (device==NULL || pt_file<0) {
		// essayer un device par defaut
		pt_setup(IOCTL_DEV_1);
		pt_file = open(IOCTL_DEV_1, O_RDWR);
		if (pt_file < 0) {
			printf("Could not open device: %s \n", IOCTL_DEV_1);
			return -1;
		}
		printf("Successfully opened device: %s\n", IOCTL_DEV_1);
	}

	if (pt_file<0) {
		// essayer un autre device par defaut
		pt_setup(IOCTL_DEV_2);
		pt_file = open(IOCTL_DEV_2, O_RDWR);
		if (pt_file < 0) {
			printf("Could not open device: %s \n", IOCTL_DEV_2);
			return -1;
		}
		printf("Successfully opened device: %s\n", IOCTL_DEV_2);
	}

	pt_sendcmd(pt_file, "FT", NULL, 0);
	return pt_file;
}

int pt_close( int file ) {
	close(file);
	return 0;
}

// Lit le contenu du buffer
int pt_read( int file, char* buf, int bufsize ) {
	int ri,i,k,error=0;
	ri=i=0;

	char* rawbuf = malloc(sizeof(char)*bufsize);
	
	// rawbuf contient les characteres de commande et de limite
	// qui sont retournes automatiquement apres chaque commande
	for ( ri=0 ; ri<bufsize ; ri++ ) {
		k = read( file, rawbuf+ri, 1 );
        printf("[%c](%d)k=%d\n",rawbuf[ri],rawbuf[ri],k);
		if ( k<=0 ) {
			rawbuf[ri] = '\0';
			break;
		}
		if ( rawbuf[ri]=='*' ) {
			k = read( file, rawbuf+(++ri), 1 );
            printf("[%c](%d)k=%d\n",rawbuf[ri],rawbuf[ri],k);
			error=0;
			break;
		}
		if ( rawbuf[ri]=='!' ) {
			k = read( file, rawbuf+(++ri), 1 );
            printf("[%c](%d)k=%d\n",rawbuf[ri],rawbuf[ri]);
			error=1;
			break;
		}
	} 

	// buf contient l'information pertinente qu'on attend
	// en retour de notre commande
	for ( i=0 ; i<bufsize ; i++ ) {
		k = read( file, buf+i, 1 );
        printf("[%c](%d)k=%d\n",rawbuf[ri],rawbuf[ri],k);
		if ( k<=0 || buf[i]=='\n') {
			buf[i] = '\0';
			break;
		}
	}
	//printf("raw READ: %s\n", rawbuf);
	//printf("READ: %s\n", buf);
	
	free(rawbuf);

	return 1;
}

// Ecrit une commande dans le buffer du device
int pt_write( int file, char* buf) {

	//printf("WRITE:%s", buf);

	int bufsize=0;
	while(buf[bufsize]!='\0')
		bufsize++;
	write( file, buf, bufsize);
	
	usleep(250);
	return 1;
}

// Envoie une commande et attend la reponse
int pt_sendcmd( int file, char* buf, char* answer, int answer_size) {
	// ajouter le charactere de fin
	char delimbuf[500];
	sprintf(delimbuf,"%s\n",buf);

	int k;
    printf("sendcmd: >> %s\n",delimbuf);
	k = pt_write(file, delimbuf);

	char tmpbuf[50];
	if (answer==NULL) {
		k = pt_read(file, tmpbuf, 500);
        printf("<< got k=%d >%s<\n",k,tmpbuf);
    } else {
		k = pt_read(file, answer, answer_size);
        printf("<< got k=%d >%s<\n",k,answer);
    }
}

// Formatte et envoie une commande de forme 'cmd<value>\n'
int pt_sendcmd_int( int file, char* cmd, int value) {
	char buf[50];
	sprintf(buf, "%s%i", cmd, value);

	return pt_sendcmd(file, buf, NULL, 0);
}

int pt_getstatus( int file, char* cmd ) {
	char buf[50];
	pt_sendcmd(file,cmd,buf,50);
	int value = atoi(buf);

	return value;
}

int pt_waitloop( int file, char* cmd, int value ) {
	
	pt_sendcmd_int(file,cmd,value);
    int c;
	while((c=pt_getstatus(file, cmd)) != value) {
	    printf("waiting... %d and %d : %d\n", c,value,c!=value);
		usleep(500000);
	}
	return 1;
}
